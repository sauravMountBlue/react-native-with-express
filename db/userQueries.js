const connection = require('./config');

const promisifyQueries = (query, data) =>
	new Promise((res, rej) => {
		connection.query(query, data, (err, result) => {
			if (err) rej(err);
			res(result);
		});
	});

const getAllUsers = () => promisifyQueries('Select profile_pic, first_name, last_name from users');
const getSingleUser = id => promisifyQueries('Select * from users where id = ?', [id]);
const updateUser = (data, id) =>
	promisifyQueries('update users set first_name = ?, last_name = ?, email = ?, contact_no = ? where id = ?', [
		...data,
		id,
	]);
const deleteUser = id => promisifyQueries('Delete from users where id = ?', [id]);
const creatingUser = data =>
	promisifyQueries(
		'insert into users(first_name, last_name, email, password, contact_no, profile_pic) values(?,?,?,?,?,?)',
		data
	);
const authenticateUser = data =>
	promisifyQueries('select email, password from users where email = ? and password = ?', data);

module.exports = {
	getAllUsers,
	getSingleUser,
	updateUser,
	deleteUser,
	creatingUser,
	authenticateUser,
};
