const mysql = require('mysql');
const connection = mysql.createConnection({
	host: process.env.DB_HOST,
	user: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	port: process.env.DB_PORT,
	database: process.env.DB_DATABASE,
});

connection.connect(err => {
	err ? console.log(err) : console.log(`Connection established with database`);
});

module.exports = connection;
