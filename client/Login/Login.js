import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Login = () => {
	return (
		<View style={styles.loginContainer}>
			<Text>{`Login Page`}</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	loginContainer: {
		flex: 1,
	},
});

export default Login;
