import { GET_USERS } from './actionTypes';
import fetchUsers from './fetchUsers';

export { GET_USERS, fetchUsers };
