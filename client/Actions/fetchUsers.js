import { GET_USERS } from './actionTypes';

const fetchUsers = data => {
	return {
		type: GET_USERS,
		data,
	};
};

export default fetchUsers;
