import React, { useEffect } from 'react';
import { StyleSheet, Text, ScrollView } from 'react-native';
import { Provider } from 'react-redux';
import store from './Store';
// import Login from './Login';
import Users from './Users';

const App = () => {
	return (
		<Provider store={store}>
			<ScrollView style={styles.container}>
				<Users />
			</ScrollView>
		</Provider>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		marginTop: 40,
	},
});

export default App;
