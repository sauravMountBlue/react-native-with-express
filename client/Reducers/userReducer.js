import { GET_USERS } from '../Actions';

const intialState = {
	users: [],
};

const userReducer = (state = intialState, action) => {
	switch (action.type) {
		case GET_USERS:
			return {
				...state,
				users: action.data,
			};
		default:
			return state;
	}
};

export default userReducer;
