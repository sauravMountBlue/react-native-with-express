import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Avatar, Card, Text } from 'react-native-paper';

const User = props => {
	const {
		user: { first_name, last_name, profile_pic },
	} = props;

	const UserAvatar = () => {
		return <Avatar.Image size={48} source={{ uri: profile_pic }} />;
	};

	return (
		<View style={styles.userContainer}>
			<Card style={styles.cardContainer}>
				<Card.Title title={`${first_name} ${last_name}`} left={UserAvatar} style={styles.titleStyle} />
			</Card>
		</View>
	);
};

const styles = StyleSheet.create({
	userContainer: {
		flex: 1,
		padding: 20,
	},
	cardContainer: {
		elevation: 10,
		padding: 16,
	},
	titleStyle: {
		justifyContent: 'space-between',
	},
});

export default User;
