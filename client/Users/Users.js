import React, { useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { Text } from 'react-native-paper';
import { connect } from 'react-redux';
import axios from 'axios';
import { fetchUsers } from '../Actions';
import User from './components/User';

const Users = props => {
	const { fetchUsersData, users } = props;
	useEffect(() => {
		fetchUsersData();
	}, []);
	return (
		<View style={styles.usersContainer}>
			{users.map((user, key) => (
				<User user={user} key={key} />
			))}
		</View>
	);
};

const styles = StyleSheet.create({
	usersContainer: {
		flex: 1,
	},
});

const mapStateToProps = state => {
	const {
		userReducer: { users },
	} = state;
	return {
		users,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		fetchUsersData: async () => {
			try {
				const fetchData = await axios.get('http://192.168.43.216:8000/users');
				dispatch(fetchUsers(fetchData.data));
			} catch (e) {
				console.log(e);
			}
		},
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);
