const express = require('express');
const createError = require('http-errors');

const router = express.Router();

const users = require('../db/userQueries');

const { getAllUsers, getSingleUser, updateUser, deleteUser } = users;

// Fetch all users
router.get('/', async (req, res, next) => {
	try {
		const result = await getAllUsers();
		res.json(result);
	} catch (e) {
		next(createError(500, e));
	}
});

// Fetch single user
router.get('/:id', async (req, res, next) => {
	const {
		params: { id },
	} = req;
	try {
		const result = await getSingleUser(id);
		if (result.length === 0) {
			next(createError(404, `No user found having id: ${id}`));
		} else {
			res.json(result);
		}
	} catch (e) {
		next(createError(500, e));
	}
});

// Update user records
router.put('/:id', async (req, res, next) => {
	const {
		params: { id },
		body,
	} = req;
	try {
		const result = await getSingleUser(id);
		if (result.length === 0) {
			next(createError(500, `No user found having id: ${id}`));
		} else {
			let { first_name, last_name, email, contact_no } = result[0];
			(first_name = body.first_name ? body.first_name : first_name),
				(last_name = body.last_name ? body.last_name : last_name),
				(email = body.email ? body.email : email),
				(contact_no = body.contact_no ? body.contact_no : contact_no);
			const updatedResult = await updateUser([first_name, last_name, email, contact_no], id);
			if (updatedResult.affectedRows > 0) {
				res.json([
					{
						first_name,
						last_name,
						email,
						contact_no,
					},
				]);
			} else {
				next(createError(500, `Error while updating user records...`));
			}
		}
	} catch (e) {
		next(createError(500, e));
	}
});

// Delete user records
router.delete('/:id', async (req, res, next) => {
	const {
		params: { id },
	} = req;
	try {
		const deleteResponse = await deleteUser(id);
		if (deleteResponse.affectedRows > 0) {
			res.send(`Successfully delete user having id: ${id}`);
		} else {
			const result = await getSingleUser(id);
			if (result.length === 0) {
				next(createError(404, `No user found having id: ${id}`));
			} else {
				next(createError(500, `Error while deleting user records...`));
			}
		}
	} catch (e) {
		next(createError(500, e));
	}
});

module.exports = router;
