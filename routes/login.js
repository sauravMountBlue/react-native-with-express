const express = require('express');
const createError = require('http-errors');

const router = express.Router();

const users = require('../db/userQueries');

const { authenticateUser } = users;

router.get('/', async (req, res, next) => {
	const {
		body: { email, password },
	} = req;
	const loginResponse = await authenticateUser([email, password]);
	if (loginResponse.length > 0) {
		res.json([{ loginSuccessful: true }]);
	} else {
		next(createError(500, 'Login failed'));
	}
});

module.exports = router;
