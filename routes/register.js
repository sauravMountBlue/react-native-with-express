const express = require('express');
const createError = require('http-errors');

const router = express.Router();

const users = require('../db/userQueries');

const { creatingUser } = users;

router.post('/', async (req, res, next) => {
	const {
		body: { first_name, last_name, email, password, contact_no },
	} = req;
	const profile_pic = req.body.profile_pic ? req.body.profile_pic : `https://www.w3schools.com/w3images/avatar2.png`;
	const registerResponse = await creatingUser([first_name, last_name, email, password, contact_no, profile_pic]);
	if (registerResponse.affectedRows > 0) {
		res.json([{ registrationSuccessful: true }]);
	} else {
		next(createError(500, 'User creation failed'));
	}
});

module.exports = router;
