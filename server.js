const express = require('express');
const app = express();
require('dotenv').config();

const errorHandler = require('./ErrorHandler');
const users = require('./routes/users');
const register = require('./routes/register');
const login = require('./routes/login');

// Body Parser
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Routes
app.use('/users', users);
app.use('/register', register);
app.use('/login', login);

// Error Handler
app.use(errorHandler);

app.listen(process.env.PORT, () => console.log(`Server is running at port: ${process.env.PORT}`));
